"use strict";

import { User } from '../api/entity/User';
import { ROLE_ADMIN } from '../api/static/Roles';
import { Config } from '../api/entity/Config';

const bcrypt = require('bcrypt');
const rl = require('readline');
const { createConnection } = require("typeorm");
const readline = rl.createInterface({
    output: process.stdout,
    input: process.stdin
})

console.log('[MiAdmin] For start the project we need to create first users in database');
readline.question("[MiAdmin] Email of Administrator: ", (email) => {
    if (!email) {
        console.log('[MiAdmin] Email is required');
        readline.close();
        process.exit(0);
    }

    readline.question("[MiAdmin] Password of Administrator: ", (password) =>  {
        if (!password) {
            console.log('[MiAdmin] Password is required');
            readline.close();
            process.exit(0);
        }

        createConnection().then((connection) => {
            console.log('[MiAdmin] Connection to the database...');
            console.log('[MiAdmin] Insert administrator...');

            let salt = bcrypt.genSaltSync(10);
            let hash = bcrypt.hashSync(password, salt);

            let user = new User();
            user.roles = ROLE_ADMIN;
            user.email = email;
            user.salt = salt;
            user.password = hash;

            connection.getRepository(User).save(user).then(() => {
                console.log('[MiAdmin] Administration has created');
                console.log('[MiAdmin] Insert default data...');

                let minMem = new Config();
                minMem.name = 'min_mem';
                minMem.value = '1024';

                let maxMem = new Config();
                maxMem.name = 'max_mem';
                maxMem.value = '1024';

                connection.getRepository(Config).save([minMem, maxMem]).then(() => {
                    console.log('[MiAdmin] default data has inserted');
                    console.log('[MiAdmin] Welcome to MiAdmin ! after run "npm start" you need to copy/paste/configure .env.dist to .env')
                    readline.close();
                    process.exit(0);
                }).catch((error) => {
                    console.log(error);
                });
            }).catch((error) => {
                console.log(error);
            });
        });
    });
});
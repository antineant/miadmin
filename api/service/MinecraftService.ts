import { WrapServer } from '../lib/WrapServer';
import { ConfigDbService } from './ConfigDbService';
import {Config} from "../entity/Config";
require('dotenv').config();

export default class MinecraftService {
    mineWrapper: WrapServer;
    configDbService: ConfigDbService;
    minMem: Config | any;
    maxMem: Config | any;

    constructor(connection) {
        this.configDbService = new ConfigDbService(connection);
        this.setNewWrapper();
    }

    setNewWrapper() {
        this.setMem().then(() => {
            this.mineWrapper = new WrapServer(process.env.MC_SERVER_JAR, process.env.MC_PATH, {
                minMem: this.minMem.value || '1024',
                maxMem: this.maxMem.value || '1024',
            });
        })
    }

    async setMem() {
        this.minMem = await this.configDbService.getMinMem();
        this.maxMem = await this.configDbService.getMaxMem();
    }

    startServer() {
        this.mineWrapper.startServer();
    }

    stopServer() {
        this.mineWrapper.stopServer();
    }

    sendCmd(cmd) {
        this.mineWrapper.writeServer(cmd);
    }
}

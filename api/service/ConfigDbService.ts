import { Repository } from "typeorm";
import { Config } from "../entity/Config";

export class ConfigDbService {
    configRepository: Repository<Config>;

    constructor(connection) {
        this.configRepository = connection.getRepository(Config);
    }

    async getMinMem() {
        let minMem = await this.configRepository.find({ where: { name: 'min_mem' }});

        if (minMem.length > 0) {
            return minMem[0];
        }

        return { value: '1024' };
    }

    async getMaxMem() {
        let maxMem = await this.configRepository.find({ where: { name: 'max_mem' }});

        if (maxMem.length > 0) {
            return maxMem[0];
        }

        return { value: '1024' };
    }
}

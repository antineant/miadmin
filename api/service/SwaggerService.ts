const swaggerJsdoc = require("swagger-jsdoc");
require('dotenv').config();

export default class SwaggerService {

    public options() {
        return swaggerJsdoc({
            definition: {
                openapi: '3.0.0',
                info: {
                    title: 'MiAdmin Express API',
                    version: '0.1.0',
                    description: 'This is MiAdmin API for administration of minecraft server',
                    license: {
                        name: 'MIT',
                        url: 'https://spdx.org/licenses/MIT.html',
                    }
                },
                servers: [
                    {
                        url: `http://localhost:${process.env.PORT}/api`,
                    }
                ]
            },
            apis: ['./api/*.ts']
        });
    }
}
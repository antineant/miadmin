import { ROLE_ADMIN as ROLE } from "../static/Roles";
import { Socket } from "socket.io";
const jwt = require('jsonwebtoken');

export function AdminAuthenticationSocket(socket: Socket) {
    let bearer = socket.request.headers.authorization;

    if (!bearer) {
        socket.emit('error', { message: 'Token has not found' } );
        socket.disconnect();
    } else {
        let token = bearer.split(' ')[1];

        if (token) {
            let user = jwt.decode(token, process.env.JWT_KEY);

            if (user.data.roles != ROLE) {
                socket.emit('error', { message: 'You not have access to this' } );
                socket.disconnect();
            }
        } else {
            socket.emit('error', { message: 'Token has not found' } );
            socket.disconnect();
        }
    }
}
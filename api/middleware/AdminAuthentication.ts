import { ROLE_ADMIN } from "../static/Roles";
import { NextFunction, Request, Response } from "express";
const jwt = require('jsonwebtoken');

export function AdminAuthentication(req: Request|any, res: Response|any, next: NextFunction|any) {
    let bearer = req.header("Authorization");

    if (bearer) {
        let token = bearer.split(' ')[1];

        if (!token) {
            res.status(401).json({message: 'Token has not found' });
        } else {
            let user = jwt.decode(token, process.env.JWT_KEY);

            if (user.data.roles == ROLE_ADMIN) {
                next()
            } else {
                res.status(401).json({message: 'You not have access for this' });
            }
        }
    }
    else {
        res.status(401).json({ message: 'Token has not found' });
    }
}
import app from '../../App';
import {getUUID, getUUIDs} from "mojang-minecraft-api";

const spawn = require('child_process').spawn;
const EventEmitter = require('events').EventEmitter;
const pidusage = require('pidusage');
const os = require('os');

export class WrapServer extends EventEmitter {
    logs = [];
    mcServer: any;
    intervalUsage: any;
    intervalPlayersOnline: any;

    constructor (MC_SERVER_JAR, MC_SERVER_PATH, OPTIONS) {
        super();
        this.MC_SERVER_JAR = MC_SERVER_JAR;
        this.MC_SERVER_PATH = MC_SERVER_PATH;
        this.OPTIONS = {
            minMem: (OPTIONS && OPTIONS.minMem) ? OPTIONS.minMem : '1024',
            maxMem: (OPTIONS && OPTIONS.maxMem) ? OPTIONS.maxMem : '1024',
        };
    }

    public stopServer () {
        if (!this.mcServer) {
            return
        }

        this.logs = [];
        this.mcServer.stdin.write('stop\n');

        clearInterval(this.intervalUsage);
        clearInterval(this.intervalPlayersOnline);

        this.mcServer.on('close', () => {
            this.mcServer = null
        });
    }

    public writeServer (line) {
        this.mcServer.stdin.write(line + '\n');
    }

    public startServer() {
        this.mcServer = spawn(this.OPTIONS.javaPath ?? 'java', [
            '-jar',
            '-Xms' + this.OPTIONS.minMem + 'M',
            '-Xmx' + this.OPTIONS.maxMem + 'M',
            this.MC_SERVER_JAR,
            'nogui'
        ], {
            stdio: 'pipe',
            detached: false,
            cwd: this.MC_SERVER_PATH
        });

        let buffer = '';
        this.mcServer.stdin.setEncoding('utf8');
        this.mcServer.stdout.setEncoding('utf8');
        this.mcServer.stderr.setEncoding('utf8');
        this.mcServer.stdout.on('data', onData);
        this.mcServer.stdin.on('data', onData);
        this.mcServer.stderr.on('data', onData);

        const self = this;

        function onData (data) {
            buffer += data;
            const lines = buffer.split('\n');
            const len = lines.length - 1;
            for (let i = 0; i < len; ++i) {
                self.mcServer.emit('line', lines[i]);

                if (data.search('players online') != '-1') {
                    self.mcServer.emit('players-online', lines[i]);
                }
            }
            buffer = lines[lines.length - 1];
        }

        this.intervalUsage = setInterval(async () => {
            if (!this.mcServer) clearInterval(this.intervalUsage);

            const stats = await pidusage(process.pid)
            stats.maxMemory = os.totalmem();
            this.mcServer.emit('usage', stats);
        }, 500);

        this.intervalPlayersOnline = setInterval(async () => {
            if (!this.mcServer) clearInterval(this.intervalPlayersOnline);
            this.writeServer('list');
        }, 60000);

        this.mcServer.on('line', (line) => {
            this.logs.push(line);
            app.io.emit('logs', line);
        });

        this.mcServer.on('usage', (usage) => {
            app.io.emit('usage', usage)
        });

        this.mcServer.on('players-online', (data) => {
            let array = data.split(':');

            if (array[4]) {
                let playersString = array[4].replace('\r','').replace(' ', '');
                let arrayPlayers = playersString.split(',')

                let canRequest = false;
                arrayPlayers.forEach((elem) => {
                    if (elem != '') canRequest = true;
                })

                if (canRequest) {
                    getUUIDs(arrayPlayers).then((data) => app.io.emit('players-online', data)).catch((error) => console.log(error));
                } else {
                    app.io.emit('players-online', arrayPlayers);
                }
            }
        })
    }
}

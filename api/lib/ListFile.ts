const fs = require('fs-extra');
const path = require('path');
require('dotenv').config();

export default class ListFile {
    path: string;

    constructor(path: string) {
        this.path = path;
    }

    load() {
        return this.getAllFiles(this.path) || [];
    }

    loadArray() {
      return this.getArrayFiles(this.path) || [];
    }

    isDirectory(directoryPath: string) {
        return fs.statSync(directoryPath).isDirectory();
    }

    isFile(filePath:string ) {
        return fs.statSync(filePath).isFile();
    }

    getFiles(filePath: string) {
       return fs.readdirSync(filePath)
          .map((name) => path.join(filePath, name))
          .filter(this.isFile)
          .map((path) => {
              const fileName = this.getName(path);
              return ({'name': fileName, 'path': path, 'writable': this.isWritable(fileName)})
          });
    }

    isWritable(name: string) {
        return ['json', 'yml', 'txt', 'toml', 'csv', 'properties'].includes(this.getExtension(name));
    }

    getExtension(name: string) {
        return name.split('.').pop();
    }

    getDirectories(directoryPath: string) {
        return fs.readdirSync(directoryPath)
          .map((name) => path.join(directoryPath, name))
          .filter(this.isDirectory);
    }

    getAllFiles(filesDirectory: string) {
        const dirs = this.getDirectories(filesDirectory);
        const files = dirs.map((dir) => this.getAllFiles(dir)).reduce((a, b) => a.concat(b), []);
        return files.concat(this.getFiles(filesDirectory));
    }

    getName(absolutePath: string) {
        return path.basename(absolutePath);
    }

    getFileContent(path: string) {
        let bitmap = fs.readFileSync(path);
        return Buffer.from(bitmap, 'base64').toString('base64');
    }

    setFileContent(path: string, content: string) {
        fs.writeFileSync(path, content, 'base64');
    }

    deleteFile(path: string) {
        fs.unlinkSync(path);
    }

    getArrayFiles(filesDirectory, architecture = []) {
      const dirs = this.getDirectories(filesDirectory);

      if (dirs) {
          let key = 0;
          dirs.forEach((dir) => {
              let element = {
                  'name': this.getName(dir),
                  'path': dir,
                  'files': this.getFiles(dir)
              };

              if (this.getDirectories(dir).length > 0) {
                  element['dir'] = this.getArrayFiles(dir, architecture[key]);
              }

              architecture.push(element);
              key++;
          })
      }

      if (filesDirectory === process.env.MC_PATH || filesDirectory === process.env.BACKUP_PATH) {
          const files = this.getFiles(filesDirectory);

          if (files) {
              files.forEach((file) => architecture.push(file));
          }
      }

      return architecture;
    }
}

import { Application } from 'express';
import { UserController } from './controller/UserController';
import { ServerController } from './controller/ServerController';
import { Connection } from 'typeorm';
import { AdminAuthentication } from './middleware/AdminAuthentication';
import { ConfigDbController } from './controller/ConfigDbController';
import { ConfigController } from './controller/ConfigController';
import PluginModsController from './controller/PluginModsController';
import BackupController from './controller/BackupController';

export class ApiRoutes {

    public init(app: Application, connection: Connection) {
        let userController = new UserController(connection);
        let serverController = new ServerController(connection);
        let configDbController = new ConfigDbController(connection);
        let configController = new ConfigController();
        let pluginsModsController = new PluginModsController();
        let backupController = new BackupController();

        /**
         * @swagger
         * /auth/register:
         *   post:
         *     tags:
         *       - Auth
         *     summary: Register new User.
         *     description: Register new User.
         *     requestBody:
         *       required: true
         *       content:
         *         application/json:
         *           schema:
         *             $ref: '#/components/schemas/NewUser'
         *     responses:
         *       200:
         *         description: Creation has success.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                     type: string
         *                     description: Message of creation.
         *                     example: User has created.
         *       406:
         *         description: User exist.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                     type: string
         *                     description: Message of error.
         *                     example: User exist.
         *       500:
         *         description: Error caused by server.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                     type: string
         *                     description: Message of error.
         *                     example: Email or password is required.
         */
        app.post('/api/auth/register', userController.register);

        /**
         * @swagger
         * /auth/users/add:
         *   post:
         *     tags:
         *       - Auth
         *     summary: Add new User.
         *     description: Add new User.
         *     requestBody:
         *       required: true
         *       content:
         *         application/json:
         *           schema:
         *             $ref: '#/components/schemas/User'
         *     responses:
         *       200:
         *         description: Creation has success.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                     type: string
         *                     description: Message of creation.
         *                     example: User has created.
         *       406:
         *         description: User exist.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                     type: string
         *                     description: Message of error.
         *                     example: User exist.
         *       500:
         *         description: Error caused by server.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                     type: string
         *                     description: Message of error.
         *                     example: Email or password is required.
         */
        app.post('/api/auth/users/add', userController.add);

        /**
         * @swagger
         * /auth/login:
         *   post:
         *     tags:
         *       - Auth
         *     summary: Login User.
         *     description: Login User.
         *     requestBody:
         *       required: true
         *       content:
         *         application/json:
         *           schema:
         *             $ref: '#/components/schemas/NewUser'
         *     responses:
         *       200:
         *         description: Login has success.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 token:
         *                     type: string
         *                     description: JWT Token.
         *       401:
         *         description: Error caused by client.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                     type: string
         *                     description: Message of error.
         *                     example: Email or password is required.
         *       500:
         *         description: Error caused by server.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                     type: string
         *                     description: Message of error.
         *                     example: Email or password is required.
         */
        app.post('/api/auth/login', userController.login);

        /**
         * @swagger
         * /auth/users/{id}:
         *   get:
         *     tags:
         *       - Auth
         *     security:
         *       - Bearer: []
         *     summary: Users item.
         *     description: User.
         *     parameters:
         *       - in: path
         *         name: id
         *         schema:
         *           type: string
         *           required: true
         *           description: The id of user
         *     responses:
         *       200:
         *         description: User has found.
         *         content:
         *           application/json:
         *             schema:
         *               type: array
         *               items:
         *                 $ref: '#/components/schemas/User'
         *       500:
         *         description: User not found.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                     type: string
         *                     description: Message of error.
         *                     example: User has not found.
         */
        app.get('/api/auth/users/:id', AdminAuthentication, userController.get);

        /**
         * @swagger
         * /auth/users/{id}:
         *   delete:
         *     tags:
         *       - Auth
         *     security:
         *       - Bearer: []
         *     summary: User remove.
         *     description: User remove.
         *     parameters:
         *       - in: path
         *         name: id
         *         schema:
         *           type: string
         *           required: true
         *           description: The id of user
         *     responses:
         *       200:
         *         description: User has been delete.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                     type: string
         *                     description: Message of error.
         *                     example: User has been delete.
         *       404:
         *         description: User has not found.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                     type: string
         *                     description: Message of error.
         *                     example: User has not found.
         */
        app.delete('/api/auth/users/:id', AdminAuthentication, userController.remove);

        /**
         * @swagger
         * /auth/users/{id}:
         *   patch:
         *     tags:
         *       - Auth
         *     security:
         *       - Bearer: []
         *     summary: User update.
         *     description: User update.
         *     requestBody:
         *       required: true
         *       content:
         *         application/json:
         *           schema:
         *             $ref: '#/components/schemas/User'
         *     parameters:
         *       - in: path
         *         name: id
         *         schema:
         *           type: string
         *           required: true
         *           description: The id of user
         *     responses:
         *       200:
         *         description: User has been updated.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                     type: string
         *                     description: Message of error.
         *                     example: User has been update.
         *       404:
         *         description: User has not found.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                     type: string
         *                     description: Message of error.
         *                     example: User is required.
         */
        app.patch('/api/auth/users/edit/:id', AdminAuthentication, userController.edit)

        /**
         * @swagger
         * /auth/users:
         *   get:
         *     tags:
         *       - Auth
         *     security:
         *       - Bearer: []
         *     summary: Users list.
         *     description: Users.
         *     responses:
         *       200:
         *         description: Login has success.
         *         content:
         *           application/json:
         *             schema:
         *               type: array
         *               items:
         *                 $ref: '#/components/schemas/NewUser'
         */
        app.get('/api/auth/users', AdminAuthentication, userController.index);

        /**
         * @swagger
         * /server/start:
         *   get:
         *     tags:
         *       - Server
         *     security:
         *       - Bearer: []
         *     summary: Server Start.
         *     description: Server Start.
         *     responses:
         *       200:
         *         description: Server started has success.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                     type: string
         *                     description: Message of success.
         *                     example: Server as begin started.
         *       501:
         *         description: Server can't start.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                     type: string
         *                     description: Message of error.
         *                     example: Server is already running.
         *
         */
        app.get('/api/server/start', AdminAuthentication, serverController.start);

        /**
         * @swagger
         * /server/stop:
         *   get:
         *     tags:
         *       - Server
         *     security:
         *       - Bearer: []
         *     summary: Stop Start.
         *     description: Stop Start.
         *     responses:
         *       200:
         *         description: Server stopped has success.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                     type: string
         *                     description: Message of success.
         *                     example: Server as begin stopped.
         *       501:
         *         description: Server can't start.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                     type: string
         *                     description: Message of error.
         *                     example: Server not running.
         *
         */
        app.get('/api/server/stop', AdminAuthentication, serverController.stop);

        /**
         * @swagger
         * /server/status:
         *   get:
         *     tags:
         *       - Server
         *     security:
         *       - Bearer: []
         *     summary: Status of server.
         *     description: Status of server.
         *     responses:
         *       200:
         *         description: Server stopped has success.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 status:
         *                     type: integer
         *                     description: 1 or 0.
         *                     example: 1
         */
        app.get('/api/server/status', AdminAuthentication, serverController.isOnline);

        /**
         * @swagger
         * /server/cmd:
         *   post:
         *     tags:
         *       - Server
         *     security:
         *       - Bearer: []
         *     summary: Server CMD.
         *     description: Server CMD.
         *     requestBody:
         *       required: true
         *       content:
         *         application/json:
         *           schema:
         *             type: object
         *             properties:
         *               cmd:
         *                 type: string
         *                 description: Cmd you want to execute to the server.
         *     responses:
         *       200:
         *         description: Send cmd correctly
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                     type: string
         *                     description: Message of success.
         *                     example: This command as been send to the server.
         *       500:
         *         description: Error caused by server.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                     type: string
         *                     description: Message of error.
         *                     example: Server not running.
         */
        app.post('/api/server/cmd', AdminAuthentication, serverController.sendCmd);

        /**
         * @swagger
         * /config:
         *   get:
         *     tags:
         *       - Config
         *     security:
         *       - Bearer: []
         *     summary: Get all Config of server.
         *     description: Get all Config of server.
         *     responses:
         *       200:
         *         description: List of config.
         *         content:
         *           application/json:
         *             schema:
         *               type: array
         *               items:
         *                 $ref: '#/components/schemas/Config'
         */
        app.get('/api/config', AdminAuthentication, configDbController.index);

        /**
         * @swagger
         * /config/{name}:
         *   get:
         *     tags:
         *       - Config
         *     security:
         *       - Bearer: []
         *     summary: Get Config of server.
         *     description: Get Config of server.
         *     parameters:
         *       - in: path
         *         name: name
         *         schema:
         *           type: string
         *           required: true
         *           description: The config name
         *     responses:
         *       200:
         *         description: Find one config.
         *         content:
         *           application/json:
         *             schema:
         *               $ref: '#/components/schemas/Config'
         *       500:
         *         description: Error config path.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                  message:
         *                    type: string
         *                    description: Error message.
         *                    example: Name parameter is required.
         */
        app.get('/api/config/:name', AdminAuthentication, configDbController.find);

        /**
         * @swagger
         * /config/{name}:
         *   patch:
         *     tags:
         *       - Config
         *     security:
         *       - Bearer: []
         *     summary: Update config of server.
         *     description: Update config of server.
         *     parameters:
         *       - in: path
         *         name: name
         *         schema:
         *           type: string
         *           required: true
         *           description: The config name
         *     requestBody:
         *       required: true
         *       content:
         *         application/json:
         *           schema:
         *             type: object
         *             properties:
         *               value:
         *                 type: string
         *                 description: Value of config.
         *     responses:
         *       200:
         *         description: Patch one config.
         *         content:
         *           application/json:
         *             schema:
         *               type: array
         *               items:
         *                 $ref: '#/components/schemas/Config'
         *       500:
         *         description: Error config path.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                  message:
         *                    type: string
         *                    description: Error message.
         *                    example: Name parameter is required.
         */
        app.patch('/api/config/:name', AdminAuthentication, configDbController.edit);

        /**
         * @swagger
         * /config/{name}:
         *   delete:
         *     tags:
         *       - Config
         *     security:
         *       - Bearer: []
         *     summary: Delete config of server.
         *     description: Delete config of server.
         *     parameters:
         *       - in: path
         *         name: name
         *         schema:
         *           type: string
         *           required: true
         *           description: The config name
         *     responses:
         *       200:
         *         description: Delete config success.
         *         content:
         *           application/json:
         *             schema:
         *               type: array
         *               items:
         *                 $ref: '#/components/schemas/Config'
         *       500:
         *         description: Error config delete.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                  message:
         *                    type: string
         *                    description: Error message.
         *                    example: Name parameter is required.
         */
        app.delete('/api/config/:name', AdminAuthentication, configDbController.delete);

        /**
         * @swagger
         * /config/{name}:
         *   post:
         *     tags:
         *       - Config
         *     security:
         *       - Bearer: []
         *     summary: Add config of server.
         *     description: Add config of server.
         *     parameters:
         *       - in: path
         *         name: name
         *         schema:
         *           type: string
         *           required: true
         *           description: The config name.
         *     requestBody:
         *       required: true
         *       content:
         *         application/json:
         *           schema:
         *             type: object
         *             properties:
         *               value:
         *                 type: string
         *                 description: Value of config.
         *     responses:
         *       200:
         *         description: Added one config.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                   type: string
         *                   description: Message of creation.
         *       500:
         *         description: Error config path.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                  message:
         *                    type: string
         *                    description: Error message.
         *                    example: Name parameter is required.
         */
        app.post('/api/config/:name', AdminAuthentication, configDbController.add);

        /**
         * @swagger
         * /config-files:
         *   get:
         *     tags:
         *       - Config Files
         *     security:
         *       - Bearer: []
         *     summary: Find all Config minecraft of server.
         *     description: Find all Config minecraft of server.
         *     responses:
         *       200:
         *         description: Config minecraft.
         *         content:
         *           application/json:
         *             schema:
         *               type: array
         *               items:
         *                 $ref: '#/components/schemas/ConfigFiles'
         */
        app.get('/api/config-files', AdminAuthentication, configController.index);

        /**
         * @swagger
         * /config-files/find:
         *   post:
         *     tags:
         *       - Config Files
         *     security:
         *       - Bearer: []
         *     summary: Find config minecraft of server.
         *     description: Find config minecraft of server.
         *     requestBody:
         *       required: true
         *       content:
         *         application/json:
         *           schema:
         *             type: object
         *             properties:
         *               path:
         *                 type: string
         *                 description: path of config file.
         *     responses:
         *       200:
         *         description: Find config file minecraft.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 content:
         *                   type: string
         *                   description: base64 of file.
         *       500:
         *         description: Error config path.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                  message:
         *                    type: string
         *                    description: Error message.
         *                    example: File with this path not exist.
         */
        app.post('/api/config-files/find', AdminAuthentication, configController.find);

        /**
         * @swagger
         * /config-files/edit:
         *   post:
         *     tags:
         *       - Config Files
         *     security:
         *       - Bearer: []
         *     summary: Edit config minecraft of server.
         *     description: Edit config minecraft of server.
         *     requestBody:
         *       required: true
         *       content:
         *         application/json:
         *           schema:
         *             type: object
         *             properties:
         *               content:
         *                 type: string
         *                 description: base64 of file.
         *               path:
         *                 type: string
         *                 description: path of file.
         *     responses:
         *       200:
         *         description: Edit config file minecraft.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 message:
         *                   type: string
         *                   description: Message of success.
         *       500:
         *         description: Error config path.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                  message:
         *                    type: string
         *                    description: Error message.
         *                    example: Content and path parameter is required.
         */
        app.post('/api/config-files/edit', AdminAuthentication, configController.edit);

        /**
         * @swagger
         * /files/{type}:
         *   get:
         *     tags:
         *       - Files
         *     security:
         *       - Bearer: []
         *     summary: Get mods/plugins of server.
         *     description: Get mods/plugins of server.
         *     parameters:
         *       - in: path
         *         name: type
         *         schema:
         *           type: string
         *           enum: [mods, plugins]
         *     responses:
         *       200:
         *         description: Added file for plugins/mods.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 name:
         *                   type: string
         *                   description: Name of file.
         *                 path:
         *                   type: string
         *                   description: Path of file.
         *                 writable:
         *                   type: boolean
         *                   description: Is writable.
         *       500:
         *         description: Error config path.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                  message:
         *                    type: string
         *                    description: Error message.
         *                    example: Types availables mods, plugins.
         */
        app.get('/api/files/:type', AdminAuthentication, pluginsModsController.index)

        /**
         * @swagger
         * /files/{type}:
         *   post:
         *     tags:
         *       - Files
         *     security:
         *       - Bearer: []
         *     summary: Add mods/plugins of server.
         *     description: Add mods/plugins of server.
         *     parameters:
         *       - in: path
         *         name: type
         *         schema:
         *           type: string
         *           enum: [mods, plugins]
         *     requestBody:
         *       required: true
         *       content:
         *         application/json:
         *           schema:
         *             type: object
         *             properties:
         *               content:
         *                 type: string
         *                 description: base64 of file.
         *               name:
         *                 type: string
         *                 description: path of file.
         *     responses:
         *       200:
         *         description: Added file for plugins/mods.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                  message:
         *                    type: string
         *                    description: Success message.
         *                    example: New mods as been added.
         *       500:
         *         description: Error config path.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                  message:
         *                    type: string
         *                    description: Error message.
         *                    example: Type, content and name parameters is required.
         */
        app.post('/api/files/:type', AdminAuthentication, pluginsModsController.add);

        /**
         * @swagger
         * /files:
         *   post:
         *     tags:
         *       - Files
         *     security:
         *       - Bearer: []
         *     summary: Delete config of server.
         *     description: Delete config of server.
         *     requestBody:
         *       required: true
         *       content:
         *         application/json:
         *           schema:
         *             type: object
         *             properties:
         *               path:
         *                 type: string
         *                 description: path of file.
         *     responses:
         *       200:
         *         description: Delete file in plugins/mods.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                  message:
         *                    type: string
         *                    description: Success message.
         *                    example: File as been delete.
         *       500:
         *         description: Error config path.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                  message:
         *                    type: string
         *                    description: Error message.
         *                    example: Path parameter is required.
         */
        app.post('/api/files', AdminAuthentication, pluginsModsController.delete);

        /**
         * @swagger
         * /backup/save:
         *   get:
         *     tags:
         *       - Backup
         *     security:
         *       - Bearer: []
         *     summary: Backup files of server.
         *     description: Backup files of server.
         *     responses:
         *       200:
         *         description: Backup files of server.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                  message:
         *                    type: string
         *                    description: Success message.
         *                    example: Backup as created with this name toto.
         *       500:
         *         description: Error backup.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                  message:
         *                    type: string
         *                    description: Error message.
         *                    example: SAVE_FOLDER or BACKUP_PATH is not set in your configuration.
         */
        app.get('/api/backup/save', AdminAuthentication, backupController.save);

        /**
         * @swagger
         * /backup/find:
         *   post:
         *     tags:
         *       - Backup
         *     security:
         *       - Bearer: []
         *     summary: Find backup of minecraft of server.
         *     description: Find backup of minecraft of server.
         *     requestBody:
         *       required: true
         *       content:
         *         application/json:
         *           schema:
         *             type: object
         *             properties:
         *               path:
         *                 type: string
         *                 description: path of config file.
         *     responses:
         *       200:
         *         description: Find backup file minecraft.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                 content:
         *                   type: string
         *                   description: base64 of file.
         *       500:
         *         description: Error config path.
         *         content:
         *           application/json:
         *             schema:
         *               type: object
         *               properties:
         *                  message:
         *                    type: string
         *                    description: Error message.
         *                    example: File with this path not exist.
         */
        app.post('/api/backup/find', AdminAuthentication, backupController.find);


        /**
         * @swagger
         * /backup:
         *   get:
         *     tags:
         *       - Backup
         *     security:
         *       - Bearer: []
         *     summary: Find all backup of minecraft of server.
         *     description: Find all backup of minecraft of server.
         *     responses:
         *       200:
         *         description: Config minecraft.
         *         content:
         *           application/json:
         *             schema:
         *               type: array
         *               items:
         *                 $ref: '#/components/schemas/ConfigFiles'
         */
        app.get('/api/backup', AdminAuthentication, backupController.index);
    }
}

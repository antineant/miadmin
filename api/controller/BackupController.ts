import { Request, Response } from 'express';
import * as AdmZip from 'adm-zip';
import * as fs from 'fs';
import ListFile from "../lib/ListFile";
require('dotenv').config();

export default class BackupController {
    static isWin = process.platform === 'win32';
    static listFile: ListFile;

    constructor() {
        BackupController.listFile = new ListFile(process.env.BACKUP_PATH);
    }

    public index(req: Request, res: Response) {
        res.json(BackupController.listFile.loadArray());
    }

    public find(req: Request, res: Response) {
        if (!req.body.path) return res.status(500).json({ message: 'Path parameters in body is required' });

        try {
            if (!fs.existsSync(req.body.path)) return res.status(500).json({ message: 'File with this path not exist' });
            return res.json({ content: BackupController.listFile.getFileContent(req.body.path)});
        } catch(err) {
            return res.status(500).json({ message: 'File with this path not exist' });
        }
    }

    public save(req: Request, res: Response) {
        if (!process.env.SAVE_FOLDER) return res.status(500).json({ message: 'SAVE_FOLDER or BACKUP_PATH is not set in your configuration' });

        const file = new AdmZip();

        let separator = '/';
        if (BackupController.isWin) separator = '\\';

        const savingFolder = process.env.SAVE_FOLDER.split(',');

        savingFolder.forEach((elem) => {
            file.addLocalFolder(process.env.MC_PATH + separator + elem, elem);
        });

        let date = new Date();
        let name = `${date.getDay()}-${date.getMonth()}-${date.getDay()}_${date.getHours()}-${date.getMinutes()}-${date.getSeconds()}.zip`;

        fs.writeFileSync(process.env.BACKUP_PATH + separator + name, file.toBuffer());

        res.status(200).json({ message: 'Backup as created with this name ' + name } );
    }
}

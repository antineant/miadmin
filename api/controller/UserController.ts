import { User } from '../entity/User';
import { Repository } from 'typeorm';
import { Request, Response } from 'express';
import { ROLE_USER } from '../static/Roles';
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

export class UserController {
    static userRepository: Repository<User>;

    constructor(connection) {
        UserController.userRepository = connection.getRepository(User);
    }

    public index(req, res: Response) {
        UserController.userRepository.find().then((news) => {
            return res.json(news);
        }).catch((error) => {
            return res.status(500).json(error);
        })
    }

    public register(req: Request, res: Response) {
        if (!req.body.email && !req.body.password) return res.status(500).json({ message: 'Email or password is required' });

        UserController.userRepository.find({ where: { email: req.body.email }}).then((value) => {
            if (value && value.length > 0) {
                return res.status(406).json({ message: 'One person with this email exist' });
            }
            else {
                let user = new User();

                let salt = bcrypt.genSaltSync(10);
                let hash = bcrypt.hashSync(req.body.password, salt);

                user.email = req.body.email;
                user.password = hash;
                user.salt = salt;
                user.roles = ROLE_USER;

                UserController.userRepository.save(user).then(() => {
                    return res.status(200).json({ message: 'User has created' });
                }).catch((error) => {
                    console.log(error);
                    return res.status(500).json({ message: 'Not working just contact admin' });
                });
            }
        }).catch((error) => {
            return res.status(500).json(error);
        })

    }

    public add(req: Request, res: Response) {
        if (!req.body.email && !req.body.password && !req.body.roles) return res.status(500).json({ message: 'Email or password or roles is required' });

        UserController.userRepository.find({ where: { email: req.body.email }}).then((value) => {
            if (value && value.length > 0) {
                return res.status(406).json({ message: 'One person with this email exist' });
            }
            else {
                let user = new User();

                let salt = bcrypt.genSaltSync(10);
                let hash = bcrypt.hashSync(req.body.password, salt);

                user.email = req.body.email;
                user.password = hash;
                user.salt = salt;
                user.roles = req.body.roles;

                UserController.userRepository.save(user).then(() => {
                    return res.status(200).json({ message: 'User has created' });
                }).catch((error) => {
                    console.log(error);
                    return res.status(500).json({ message: 'Not working just contact admin' });
                });
            }
        }).catch((error) => {
            return res.status(500).json(error);
        })
    }

    public get(req: Request, res: Response) {
        if (!req.params.id) return res.status(500).json({ message: 'Id parameter is required' })

        UserController.userRepository.find({ where: { id: req.params.id }}).then((value) => {
            if (value && value.length > 0) {
                let user = value[0];

                return res.status(200).json(user);
            } else {
                return res.status(404).json({message: 'User not found'});
            }
        });
    }

    public remove(req: Request, res: Response) {
        if (!req.params.id) return res.status(500).json({ message: 'Id parameter is required' })

        UserController.userRepository.find({ where: { id: req.params.id }}).then((value) => {
            if (value && value.length > 0) {
                let user = value[0];

                UserController.userRepository.remove(user).then(() => {
                    return res.status(200).json({ message: 'User has been deleted' });
                });
            }
            else {
                return res.status(404).json({message: 'User not found'});
            }
        });
    }

    public edit(req: Request, res: Response) {
        if (!req.params.id) return res.status(500).json({ message: 'Id parameter is required' })
        if (!req.body.id || !req.body.email || (!req.body.hasOwnProperty('roles') || req.body.roles === '')) return res.status(500).json({ message: 'User parameter is required' })

        UserController.userRepository.find({ where: { id: req.params.id }}).then((value) => {
            if (value && value.length > 0) {
                let user = value[0];
                user.email = req.body.email;
                user.roles = req.body.roles;

                if (req.body.password) {
                    let salt = bcrypt.genSaltSync(10);
                    let hash = bcrypt.hashSync(req.body.password, salt);

                    user.password = hash;
                    user.salt = salt;
                }

                UserController.userRepository.save(user).then(() => {
                    return res.status(200).json({ message: 'User has been edit' });
                });
            }
            else {
                return res.status(404).json({ message: 'User not found' });
            }
        });
    }

    public login(req: Request, res: Response) {
        if (!req.body.email && !req.body.password) return res.status(401).json({ message: 'Email or password is required' });

        UserController.userRepository.find({ where: { email: req.body.email }}).then((value) => {
            if (value && value.length > 0) {
                let user = value[0];
                let compare = bcrypt.compareSync(req.body.password, user.password);

                if (compare) {
                    let token = jwt.sign({ data: user }, process.env.JWT_KEY);
                    return res.status(200).json({ token: token });
                }
                else {
                    return res.status(401).json({ message: 'Email or password is not correct' });
                }
            }
            else {
                return res.status(401).json({ message: 'Email or password is not correct' });
            }
        }).catch((error) => {
            return res.status(500).json(error);
        })
    }

}

import { Request, Response } from 'express';
import { Repository } from 'typeorm';
import { Config } from '../entity/Config';

export class ConfigDbController {
    static configRepository: Repository<Config>;

    constructor(connection) {
        ConfigDbController.configRepository = connection.getRepository(Config);
    }

    public index(req: Request, res: Response) {
        ConfigDbController.configRepository.find().then((config) => {
            return res.json(config);
        }).catch((error) => {
            return res.status(500).json(error);
        })
    }

    public find(req: Request, res: Response) {
        if (!req.params.name) return res.status(500).json({ message: 'Name parameter is required' })

        ConfigDbController.configRepository.find({ where: { 'name': req.params.name }}).then((config) => {
            if (config && config.length > 0) {
                return res.json(config[0]);
            }
            else {
                return res.status(500).json({ message: 'Config with this name not exist' });
            }
        }).catch((error) => {
            return res.status(500).json(error);
        })
    }

    public add(req: Request, res: Response) {
        if (!req.params.name) return res.status(500).json({ message: 'Name parameter is required' })
        if (!req.body.value) return res.status(500).json({ message: 'Value parameter is required' })

        ConfigDbController.configRepository.find({ where: { 'name': req.params.name }}).then((config) => {
            if (config && config.length > 0) {
                res.status(500).json({ message: 'Config with this name exist' });
            }
            else {
                let newConfig = new Config();
                newConfig.name = req.params.name;
                newConfig.value = req.body.value;

                ConfigDbController.configRepository.save(newConfig).then(() => {
                    return res.status(200).json({ message: 'Config has created' });
                }).catch((error) => {
                    console.log(error);
                    return res.status(500).json({ message: 'Not working just contact admin' });
                });
            }
        }).catch((error) => {
            return res.status(500).json(error);
        })
    }

    public edit(req: Request, res: Response) {
        if (!req.params.name) return res.status(500).json({ message: 'Name parameter is required' })
        if (!req.body.value) return res.status(500).json({ message: 'Value parameter is required' })

        ConfigDbController.configRepository.find({ where: { 'name': req.params.name }}).then((config) => {
            if (config && config.length > 0) {
                let newConfig = new Config();
                newConfig.id = config[0].id;
                newConfig.name = req.params.name;
                newConfig.value = req.body.value;

                ConfigDbController.configRepository.save(newConfig).then(() => {
                    return res.status(200).json({ message: 'Config has updated' });
                }).catch((error) => {
                    console.log(error);
                    return res.status(500).json({ message: 'Not working just contact admin' });
                });

            }
            else {
                res.status(500).json({ message: 'Config doesnt not exist'});
            }
        }).catch((error) => {
            return res.status(500).json(error);
        })
    }

    public delete(req: Request, res: Response) {
        if (!req.params.name) return res.status(500).json({ message: 'Name parameter is required' })

        ConfigDbController.configRepository.find({ where: { 'name': req.params.name }}).then((config) => {
            if (config && config.length > 0) {
                ConfigDbController.configRepository.remove(config[0]).then(() => {
                    return res.status(200).json({ message: 'Config has delete' });
                }).catch((error) => {
                    console.log(error);
                    return res.status(500).json({ message: 'Not working just contact admin' });
                });

            }
            else {
                res.status(500).json({ message: 'Config doesnt not exist'});
            }
        }).catch((error) => {
            return res.status(500).json(error);
        })

    }
}
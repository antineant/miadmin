import { Request, Response } from 'express';
import ListFile from '../lib/ListFile';
const fs = require('fs');
require('dotenv').config();

export class ConfigController {
    static listFile: ListFile;

    constructor() {
        ConfigController.listFile = new ListFile(process.env.MC_PATH);
    }

    public index(req: Request, res: Response) {
        res.json(ConfigController.listFile.loadArray());
    }

    public find(req: Request, res: Response) {
        if (!req.body.path) return res.status(500).json({ message: 'Path parameters in body is required' });

        try {
            if (!fs.existsSync(req.body.path)) return res.status(500).json({ message: 'File with this path not exist' });
            return res.json({ content: ConfigController.listFile.getFileContent(req.body.path)});
        } catch(err) {
            return res.status(500).json({ message: 'File with this path not exist' });
        }
    }

    public edit(req: Request, res: Response) {
        if (!req.body.path || !req.body.content) return res.status(500).json({ message: 'Content and path parameter is required' });

        try {
            if (!fs.existsSync(req.body.path)) return res.status(500).json({ message: 'File with this path not exist' });
            ConfigController.listFile.setFileContent(req.body.path, req.body.content);

            return res.json({ message: 'You file as been edited' });
        } catch(err) {
            return res.status(500).json({ message: 'Error, contact admin' });
        }
    }
}

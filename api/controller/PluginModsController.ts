import { Request, Response } from 'express';
import ListFile from '../lib/ListFile';
require('dotenv').config();

export default class PluginModsController {
    static listFile: ListFile;
    static isWin = process.platform === 'win32';

    constructor() {
        PluginModsController.listFile = new ListFile(process.env.MC_PATH);
    }

    public index(req: Request, res: Response) {
        if (!req.params.type) return res.status(500).json({ message: 'Type parameter is required' });
        if (!['mods', 'plugins'].includes(req.params.type)) return res.status(500).json( { message: 'Types availables: mods, plugins' });

        let typePath = process.env.MC_PLUGINS_PATH;
        if (req.params.type === 'mods') typePath = process.env.MC_MODS_PATH;

        res.status(200).json(PluginModsController.listFile.getFiles(typePath));
    }

    public add(req: Request, res: Response) {
        if (!req.params.type || !req.body.content || !req.body.name) return res.status(500).json( { message: 'Type, content and name parameters is required' });

        let typePath = process.env.MC_PLUGINS_PATH;
        if (req.params.type === 'mods') typePath = process.env.MC_MODS_PATH;

        let path = typePath + '/' + req.body.name;
        if (PluginModsController.isWin) path = typePath + '\\' + req.body.name;

        PluginModsController.listFile.setFileContent(path, req.body.content);

        res.status(200).json({ message: `New ${req.params.type} as been added` });
    }

    public delete(req: Request, res: Response) {
        if (!req.body.path) return res.status(500).json( { message: 'Path parameter is required' });

        PluginModsController.listFile.deleteFile(req.body.path);

        res.status(200).json({ message: `File as been delete` });
    }
}
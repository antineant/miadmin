import { Request, Response } from 'express';
import MinecraftService from '../service/MinecraftService';

export class ServerController {
    static wrapper: MinecraftService

    constructor(connection) {
        ServerController.wrapper = new MinecraftService(connection);
    }

    public start(req: Request, res: Response) {
        if (ServerController.wrapper.mineWrapper.mcServer) return res.status(501).json({ message: 'Server is already running'});

        ServerController.wrapper.setMem();
        ServerController.wrapper.startServer();
        res.status(201).json({ message: 'Server as begin started'});
    }

    public stop(req: Request, res: Response) {
        if (!ServerController.wrapper.mineWrapper.mcServer) return res.status(501).json({ message: 'Server not running'});

        ServerController.wrapper.stopServer();
        res.status(201).json({ message: 'Server as begin stopped'});
    }

    public sendCmd(req: Request, res: Response) {
        if (!req.body.cmd) return res.status(500).json({ message: 'cmd parameter is required' });

        if (!ServerController.wrapper.mineWrapper.mcServer) return res.status(501).json({ message: 'Server not running'});

        ServerController.wrapper.sendCmd(req.body.cmd)
        res.status(201).json( { message: 'This command as been send to the server' });
    }

    public isOnline(req: Request, res: Response) {
        if (!ServerController.wrapper.mineWrapper.mcServer) return res.status(200).json({ status: 0 })

        return res.status(200).json({ status: 1 })
    }
}


/**
 * @swagger
 * components:
 *   securitySchemes:
 *     Bearer:
 *       type: http
 *       scheme: bearer
 *       name: Authorization
 *   security:
 *     - Bearer
 *   schemas:
 *     ConfigFiles:
 *       type: object
 *       properties:
 *         name:
 *           type: string
 *           description: Name of file or directory.
 *         path:
 *           type: string
 *           description: Path of file or directory.
 *         files:
 *           type: array
 *           items:
 *             properties:
 *               name:
 *                 type: string
 *                 description: Name of file.
 *               path:
 *                 type: string
 *                 description: Path of file.
 *               writable:
 *                 type: boolean
 *                 description: Is writable.
 *         dir:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/ConfigFiles'
 *       example:
 *         name: foo.txt
 *         path: /Users/admin/foo.txt
 *         files:
 *           - name: foo.txt
 *             path: /Users/admin/foo.txt
 *             writable: true
 *         dir:
 *           - name: foo
 *             path: /Users/admin/foo
 *             files:
 *               - name: foo.txt
 *                 path: /Users/admin/foo.txt
 *     Config:
 *       type: object
 *       properties:
 *         id:
 *           type: integer
 *           description: Id of config.
 *         name:
 *           type: string
 *           description: Name of config.
 *         value:
 *           type: string
 *           description: Value of config.
 *     User:
 *       type: object
 *       properties:
 *         id:
 *           type: integer
 *           description: Id of user.
 *           example: 0
 *         salt:
 *           type: string
 *           description: Salt of password.
 *         roles:
 *           type: integer
 *           description: Role of user.
 *           example: 1
 *         email:
 *           type: string
 *           description: The email of user.
 *           example: toto@toto.fr
 *         password:
 *           type: string
 *           description: The password of user.
 *           example: azerty
 *     NewUser:
 *       type: object
 *       properties:
 *         email:
 *           type: string
 *           description: The email of user.
 *           example: toto@toto.fr
 *         password:
 *           type: string
 *           description: The password of user.
 *           example: azerty
 */
import * as express from 'express';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import 'reflect-metadata';
import { createServer } from 'http';
import { Server, Socket } from 'socket.io';
import { ApiRoutes } from './api/ApiRoutes';
import { createConnection } from 'typeorm';
import { AdminAuthenticationSocket } from './api/middleware/AdminAuthenticationSocket';
import { ConfigDbService } from './api/service/ConfigDbService';
import SwaggerService from "./api/service/SwaggerService";
import { serve, setup } from "swagger-ui-express";
import {ServerController} from "./api/controller/ServerController";
require('dotenv').config();

class App {
    public serv: express.Application;
    public apiRoutes: ApiRoutes = new ApiRoutes();
    public session: any;
    public configDbService: ConfigDbService;
    public io: Server;
    public corsOptions = {
        origin: process.env.ORIGIN,
        methods: "GET,HEAD,PUT,PATCH,POST,DELETE"
    }

    constructor() {
        this.serv = express();
        this.config();

        this.session = createServer(this.serv);
        this.io = new Server(this.session, {
            cors: {
                origin: process.env.ORIGIN,
            }
        });

        this.io.on('connection', (socket: Socket) => {
            socket.on('refresh-buffer', () => {
                if (ServerController.wrapper.mineWrapper) {
                    socket.emit('refreshed-buffer', ServerController.wrapper.mineWrapper.logs);
                }
            });

            socket.on('refreshed-buffer', (buffer) => {
                socket.emit('refreshed-buffer', buffer);
            });

            socket.on('error', (message: any) => {
                socket.emit('error', message);
            });

            AdminAuthenticationSocket(socket);

            socket.on('logs', (message: any) => {
                socket.emit('logs', message);
            });

            socket.on('usage', (usage: any) => {
                socket.emit('usage', usage);
            })

            socket.on('players-online', (players: any) => {
                socket.emit('players-online', players);
            });
        });

        createConnection().then((value) => {
            console.log('Listening database');
            this.apiRoutes.init(this.serv, value);
            this.configDbService = new ConfigDbService(value);
        });
    }

    private config(): void {
        this.serv.use(bodyParser.json({ limit: '50mb' }));
        this.serv.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
        this.serv.use(cors(this.corsOptions));

        if (process.env.USE_DOC) {
            let swaggerSpecs = new SwaggerService().options();
            this.serv.use('/docs', serve, setup(swaggerSpecs));
        }
    }
}

const PORT = process.env.PORT || 3000;

const app = new App();
app.session.listen(PORT, () => console.log(`API listening on port ${PORT}!`));

export default app;

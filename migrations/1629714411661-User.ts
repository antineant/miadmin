import {MigrationInterface, QueryRunner} from "typeorm";

export class User1629714411661 implements MigrationInterface {
    name = 'User1629714411661'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "temporary_users" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "email" varchar NOT NULL, "password" varchar NOT NULL, "roles" integer NOT NULL, "salt" varchar NOT NULL)`);
        await queryRunner.query(`INSERT INTO "temporary_users"("id", "email", "password", "roles") SELECT "id", "email", "password", "roles" FROM "users"`);
        await queryRunner.query(`DROP TABLE "users"`);
        await queryRunner.query(`ALTER TABLE "temporary_users" RENAME TO "users"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" RENAME TO "temporary_users"`);
        await queryRunner.query(`CREATE TABLE "users" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "email" varchar NOT NULL, "password" varchar NOT NULL, "roles" integer NOT NULL)`);
        await queryRunner.query(`INSERT INTO "users"("id", "email", "password", "roles") SELECT "id", "email", "password", "roles" FROM "temporary_users"`);
        await queryRunner.query(`DROP TABLE "temporary_users"`);
    }

}

# MiAdmin

![License](https://img.shields.io/badge/maintained-yes-green)
[![License](https://img.shields.io/badge/Licence-MIT-blue)](LICENSE.md)
![License](https://img.shields.io/badge/Node-14-red)

Administration of minecraft server (mods and vanilla) in node.JS

If you want UI associated to this projet you can use: https://gitlab.com/antineant/miadmin-ui

# Requirement

- Node 14 (minimum)

# Functionality

- Start/Stop server
- Cmd to the server with socket
- Logs with real time (with socket.io)
- Configuration file edition
- Run configuration edition
- Backup in .zip
- List plugins/mods

# Installation

```
git clone git@gitlab.com:antineant/miadmin.git
```


After installation of node you can run this cmd for install the project

```
npm run installation
```

Now you need to copy/paste .env.dist in .env and you need to configure this.

# Included Commands

|Command|Description|
| ---- | ---- |
|`npm run start`| Execute the app |
|`npm run typeorm`| Access to typeorm cmd |
|`npm run typeorm:migrate`| Create migration from api/entity |
|`npm run typeorm:run`| Execute migration from migrations/ |
|`npm run installation`| Command on the first start |

# Api Documentation

## Basic API
If you enabled `USE_DOC` in .env you can access to the documentation in `/docs` in your best browser

## Socket IO

We use socket IO for emit:
- logs (of minecraft server)
- error
- usage (cpu/ram)

For authentification of socket.io, you need to use extraHeaders with JWT from `/api/auth/login` like this (example) :

```js
const socket = io("http://localhost:8000", {
    extraHeaders: {
        Authorization: 'Bearer TOKEN'
    }
});

socket.on('logs', function(message) {console.log(message)})
socket.on('error', function(message) {console.log(message)})
socket.on('usage', function(message) {console.log(message)})
```

# Creator

- Anwoon




